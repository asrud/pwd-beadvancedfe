import Axios from "axios";
import { API_URL } from "../../endpoint/API";

export const registerUser = ({ fullName, userName, password, email }) => {
  return (dispatch) => {
    Axios.post(`${API_URL}/users`, {
      fullName,
      userName,
      email,
      password,
      role: "user",
      deliveryList: [],
    })
      .then((res) => {
        delete res.data.password;
        dispatch({
          type: "AUTH_USER",
          payload: res.data,
        });
        alert("daftar user berhasil");
      })
      .catch((err) => {
        alert("terjadi masalah pada server");
      });
  };
};

export const loginUser = ({ email, password }) => {
  return (dispatch) => {
    Axios.post(`${API_URL}/users/login`, {
      email,
      password,
    })
      .then((res) => {
        localStorage.setItem("token_shutter", res.data.token);
        dispatch({
          type: "AUTH_USER",
          payload: res.data.dataLogin,
        });
        alert("success login");
      })
      .catch((err) => {
        alert("failed login");
      });
  };
};

export const logOut = () => {
  return (dispatch) => {
    dispatch({
      type: "LOG_OUT",
    });
  };
};
