import React from "react";
import "../assets/style/home.css";
import { FiShoppingCart } from "react-icons/fi";
import Axios from "axios";
import { API_URL } from "../endpoint/API";

class Home extends React.Component {
  state = {
    addFileName: "",
    addFile: [],
    dataEmail: [],
    dataAlbum: [],
  };

  componentDidMount() {
    this.getDataAlbum();
  }

  getDataAlbum = () => {
    Axios.get(`${API_URL}/album/get`).then((res) => {
      this.setState({
        dataAlbum: res.data,
      });
    });
  };

  onBtAdd = () => {
    if (this.state.addFile) {
      let formData = new FormData();
      let obj = {
        title: this.inputTitle.value,
        description: this.inputDescription.value,
      };

      formData.append("data", JSON.stringify(obj));
      formData.append("file", this.state.addFile);
      Axios.post(`${API_URL}/album/upload`, formData)
        .then((res) => {
          alert(res.data.message);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  onBtAddFile = (e) => {
    if (e.target.files[0]) {
      this.setState({
        addFileName: e.target.files[0].name,
        addFile: e.target.files[0],
      });
      let preview = document.getElementById("imgpreview");
      preview.src = URL.createObjectURL(e.target.files[0]);
    }
  };
  render() {
    return (
      <div className="container-fluid page-style">
        <div className="row d-flex justify-content-center">
          {/* side bar section */}
          <div className="col-2 my-3 ">
            <div class="card mb-3">
              <div class="card-header">Add Data</div>
              <div class="card-body d-grid gap-3">
                <div>
                  <img id="imgpreview" width="100%" />
                </div>
                <div>
                  <div class="input-group mb-3">
                    <input
                      type="file"
                      class="form-control"
                      id="inputGroupFile02"
                      onChange={this.onBtAddFile}
                    />
                  </div>
                </div>
                <div>
                  <label for="product-name">Title</label>
                  <input
                    class="form-control"
                    type="text"
                    id="product-name"
                    placeholder="masukkan title"
                    ref={(el) => (this.inputTitle = el)}
                  />
                </div>
                <div>
                  <label for="product-cath">Description</label>
                  <input
                    class="form-control"
                    type="text"
                    id="product-name"
                    placeholder="masukkan title"
                    ref={(el) => (this.inputDescription = el)}
                  />
                </div>
                <button className="btn btn-primary" onClick={this.onBtAdd}>
                  Add Data
                </button>
              </div>
            </div>
            <div class="card">
              <div class="card-header">Sort Product</div>
              <div class="card-body d-grid gap-3">
                <div>
                  <label for="product-sort">Urutkan Berdasarkan</label>
                  <select class="form-control form-control" id="product-sort">
                    <option>Default</option>
                    <option>Lowest Price</option>
                    <option>Highest Price</option>
                    <option>A-Z</option>
                    <option>Z-A</option>
                  </select>
                </div>
                <button className="btn btn-primary">Urutkan Produk</button>
              </div>
            </div>
          </div>
          {/* product section */}
          <div className="col-8">
            <div className="d-flex flex-wrap">
              {this.state.dataAlbum.map((item) => {
                return (
                  <div className="card m-3" style={{ width: "18rem" }}>
                    <div className="pic-box">
                      <img
                        className="card-img-top"
                        src={API_URL + item.image}
                        alt="Card image cap"
                      />
                    </div>
                    <div className="card-body">
                      <h6 className="prod-name">{item.title}</h6>
                      <p className="desc">{item.description}</p>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
