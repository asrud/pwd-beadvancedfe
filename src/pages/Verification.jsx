import React, { Component } from "react";
import Axios from "axios";
import { API_URL } from "../endpoint/API";

class Verification extends Component {
  state = {
    message: "Loading ...",
  };

  componentDidMount() {
    Axios.patch(
      `${API_URL}/users/verified`,
      {},
      {
        headers: {
          Authorization: `Bearer ${this.props.match.params.token}`,
        },
      }
    )
      .then((res) => {
        this.setState({
          message: "your account verified",
        });
      })
      .catch((err) => {
        alert(err);
        console.log(err);
      });
  }

  render() {
    return (
      <div className="container-fluid page-style">
        <h2>{this.state.message}</h2>
      </div>
    );
  }
}

export default Verification;
